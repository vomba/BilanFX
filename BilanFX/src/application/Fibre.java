package application;

public class Fibre {

	public double idref; //indice refraction
	public double diffindrel; //diiference indice relatif
	public double diamcoeur; //diametre du coeur nm
	public double coeffatt; // coefficient d'attenuation db/Km
	public double percon; // perte connecteur dB
	public int nepiss; // nombre des epissures
	public double perepiss; // perte epissures dB
	public double coeffdisp; // coefficient dispertion chrom PS/Km nm
	public double msec; // marge securite dB

	public Fibre()
	{}
}
