package application;


public class Calcule  {

	private final double c=3*Math.pow(10, 8);
	private final double h=(6.626)*Math.pow(10, -34);
	
	public Calcule()
	{}


	public double mu(double idrefra, double longonde){
		return c/(idrefra*longonde*Math.pow(10,-9));
	}

	public double v(double idrefra,double diamcoeur, double longonde, double diffrela)
	{
		return (Math.PI*idrefra*diamcoeur*Math.pow(10,3)*Math.sqrt(diffrela*2))/longonde;
	}
	
	public double pr(double sens, double mu, double D)
	{
		return sens*this.h*mu*D*Math.pow(10,9);
	}



	public double l1(double coeffatt,double puissinj, double percon, double perepiss, int nepiss, double msec, double pr)
	{
		return (10*Math.log(puissinj)-2*percon-nepiss*perepiss-msec-10*Math.log(pr))/coeffatt;
	}

	public double l2m(double coefdisschrom, double largspect, double debit)
	{
		return 1/(4*Math.abs(coefdisschrom)*largspect*debit*Math.pow(10,-6));
	}

	public double l2s(double idrefra, double idrela, double debit)
	{
		return this.c/(idrefra*idrela*4*debit*Math.pow(10,9));
	}

	public double l2g(double idrefra, double idrela, double debit)
	{
		return 2*this.c/(idrefra*Math.pow(idrela,2)*debit*Math.pow(10,9));
	}
}
