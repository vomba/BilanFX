package application;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;

import java.net.URL;
import java.text.DecimalFormat;
import java.util.ResourceBundle;

public class BilanController implements Initializable{

    Fibre f=new Fibre();
    EmitRec er=new EmitRec();
    Calcule c=new Calcule();
    boolean mode, gin=false, sin=false;
    DecimalFormat formatter = new DecimalFormat("#0.000");

    @FXML
    private TextField idrela;

    @FXML
    private TextField sens;

    @FXML
    private TextField coeffdispchrom;

    @FXML
    private TextField perepiss;

    @FXML
    private TextField percon;

    @FXML
    private TextField nepiss;

    @FXML
    private AnchorPane basepane;

    @FXML
    private RadioButton sindice = new RadioButton("saut");

    @FXML
    private RadioButton momode;

    @FXML
    private TextField largspect;

    @FXML
    private TextField coeffattchrom;

    @FXML
    private TextField idrefra;

    @FXML
    private RadioButton mumode;

    @FXML
    private Text result;

    @FXML
    private TextField pe;

    @FXML
    private TextField msec;

    @FXML
    private RadioButton gindice= new RadioButton("gradient");

    @FXML
    private TextField longonde;

    @FXML
    private Button calc;

    @FXML
    private TextField debit;

    @FXML
    private ToggleGroup tg2;

    @FXML
    private TextField diamcoeur;

    @FXML
    private ToggleGroup tg1;


    void Init()
    {
        f.idref=Double.parseDouble(idrefra.getText());
        f.coeffatt=Double.parseDouble(coeffattchrom.getText());
        f.coeffdisp=Double.parseDouble((coeffdispchrom.getText()));
        f.diamcoeur=Double.parseDouble((diamcoeur.getText()));
        f.diffindrel=Double.parseDouble(idrela.getText());
        f.msec=Double.parseDouble(msec.getText());
        f.nepiss= Integer.parseInt(nepiss.getText());
        f.perepiss=Double.parseDouble(perepiss.getText());
        f.percon=Double.parseDouble(percon.getText());
        er.debnum=Double.parseDouble(debit.getText());
        er.largspect=Double.parseDouble(largspect.getText());
        er.longonde=Double.parseDouble(longonde.getText());
        er.puissinject=Double.parseDouble(pe.getText());
        er.sens=Double.parseDouble(sens.getText());
    }


    public BilanController()
    {
        this.tg1=new ToggleGroup();
        this.tg2=new ToggleGroup();

    }


    @FXML
    private void setmode()
    {
        double s;
        Init();
        s=c.v(f.diamcoeur,f.idref,er.longonde,f.diffindrel);
        if(s<2.405)
        {
            momode.setSelected(true);
            mumode.setSelected(false);
            mode=true;
        }

        if(s>=2.405)
        {

            mumode.setSelected(true);
            momode.setSelected(false);
            mode=false;
        }


    }


    @FXML
    private void Calculate(ActionEvent event) {

        double mu=c.mu(f.idref,er.longonde);
        double pr=c.pr(er.sens,mu,er.debnum);
        double l1=c.l1(f.coeffatt,er.puissinject,f.percon,f.perepiss,f.nepiss,f.msec,pr);
        double l2=0,lf=0;
        if(mode==true){
            l2=c.l2m(f.coeffdisp,er.largspect,er.debnum);
        }
        else if(mode==false)
        {
            if(gin==false && sin==true)
            {
                l2=c.l2s(f.idref,f.diffindrel,er.debnum);
            }
            else if(gin==true && sin==false)
            {
                l2=c.l2g(f.idref,f.diffindrel,er.debnum);
            }
        }

        if(l2<=l1)
        {
            lf=l2;
        }
        else if(l2>l1)
        {
            lf=l1;
        }

        result.setText("La Distance Maximale="+formatter.format(lf)+" Km ");
    }



    @Override
    public void initialize(URL url, ResourceBundle rb)
   {
        sindice.setToggleGroup(tg2);
        sindice.setUserData("saut");
        gindice.setToggleGroup(tg2);
        gindice.setUserData("gradient");

        tg2.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {

            public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
                if(tg2.getSelectedToggle()!=null)
                {
                    if(tg2.getSelectedToggle().getUserData().toString().equalsIgnoreCase("saut")) {
                        sin = true;
                        gin = false;
                    }
                        else if(tg2.getSelectedToggle().getUserData().toString().equalsIgnoreCase("gradient")) {
                        sin = false;
                        gin = true;
                    }
                }
            }
        });
   }
}
